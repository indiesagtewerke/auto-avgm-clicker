# Auto A.V.G.M. Clicker

Auto click through the game 'A.V.G.M.' created by Edmund McMillen. The game is part of The Basement Collection. A.V.G.M.is also available on Kongregate and Newgrounds.

## Requirements

- Windows operating system
- [AutoIt v3](https://www.autoitscript.com/site/) installed

## How to use

1. Open 'The Basement Collection' and start A.V.G.M. by selecting the desired game mode (normal, nightmare, hell, inferno).
2. Run this clicker application script `"C:\Program Files (x86)\AutoIt3\AutoIt3.exe" Auto-AVGM-Clicker.au3`
3. Close the dailog that confirms that the script started.
4. Switch back to A.V.G.M. and click OK to start flipping the switch.
5. Press the 'Esc' key to stop the script's execution earlier than the specified amount of clicks in the script.

Press ESC any time to stop the script's execution.

## Tools used for Development

- Click automation developed with the [AutoIt3][autoit] scripting language.
- GUI Prototyping with [Pencil][pencil].
- The GUI implementation done with [Koda Form Designer][koda].

[autoit]: https://www.autoitscript.com/site/
[pencil]: https://pencil.evolus.vn/
[koda]: http://koda.darkhost.ru/page.php?id=index
