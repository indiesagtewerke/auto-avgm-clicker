#cs ----------------------------------------------------------------------------

  AutoIt Version: 3.3.14.5
  Author:	Quintin Henn
  version:	1.1.0

  Script Function:
  Auto click through Abusive Video Game Manipulation

  WARNING: This program may potentially trigger seizures for people with photosensitive epilepsy. User discretion is advised.

#ce ----------------------------------------------------------------------------

#cs ----------------------------------------------------------------------------

  HOW TO USE

  1. Open 'The Basement Collection' and start A.V.G.M. by selecting the desired game mode (normal, nightmare, hell, inferno)
  2. Run this clicker application script
  3. Switch back to A.V.G.M. and click OK to start flipping the switch

  Press ESC any time to stop the script's execution

#ce ----------------------------------------------------------------------------

#include <MsgBoxConstants.au3>
#include <Misc.au3>

#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>

Const $appName = "Auto A.V.G.M. Clicker"

Const $normalClicks = 10212
Const $nightmareClicks = 22450
Const $hellClicks = 49148
Const $infernoClicks = 107175

Const $NotStartedState = "Not Yet Started"
Const $InProgressState = "In Progress"
Const $CompletedState = "Completed"

; TODO: read game mode from command line:
Global $modeParam = $CmdLine[0] = 1 ? $CmdLine[1] : "Normal"
Global $mode = "Default"
Global $clicks = $normalClicks
Global $clickCount = 1
Global $escapeKeyCode = "1B"

Global $mouseX = 1485
Global $mouseY = 468

Global $hDLL = DllOpen("user32.dll")

#Region ### START Koda GUI section ### Form=avgmClickerForm.kxf
$avgmClickerForm = GUICreate("Auto A.V.G.M. Clicker", 345, 336, 704, 270)
$modeNormal = GUICtrlCreateRadio("Normal", 32, 112, 113, 17)
$modeNightmare = GUICtrlCreateRadio("Nightmare", 32, 144, 113, 17)
$modeHell = GUICtrlCreateRadio("Hell", 32, 176, 113, 17)
$modeInferno = GUICtrlCreateRadio("Inferno", 32, 208, 113, 17)

Select
  Case $modeParam = "-n"
    SetNormalMode()
	  GUICtrlSetState($modeNormal, $GUI_CHECKED)
  Case $modeParam = "-m"
    SetNightmareMode()
    GUICtrlSetState($modeNightmare, $GUI_CHECKED)
  Case $modeParam = "-h"
    SetHellMode()
    GUICtrlSetState($modeHell, $GUI_CHECKED)
  Case $modeParam = "-i"
    SetInfernoMode()
    GUICtrlSetState($modeInferno, $GUI_CHECKED)
  Case Else
    SetNormalMode()
	  GUICtrlSetState($modeNormal, $GUI_CHECKED)
EndSelect

$step2Group = GUICtrlCreateGroup("Step 2: Select the game mode:", 16, 88, 313, 153)
GUICtrlCreateGroup("", -99, -99, 1, 1)

$helpButton = GUICtrlCreateButton("Help", 16, 296, 75, 25)
$exitButton = GUICtrlCreateButton("Exit", 160, 296, 75, 25)
$startButton = GUICtrlCreateButton("Start", 248, 296, 75, 25)
SetStatus($NotStartedState)
$step1Label = GUICtrlCreateLabel("Step1: Select to light switch coordinates:", 24, 32, 196, 17)
$selectButton = GUICtrlCreateButton("Select", 24, 56, 75, 25)
$coordinatesLabel = GUICtrlCreateLabel("X: <x>, Y: <y>", 112, 56, 214, 17)
$avgmLabel = GUICtrlCreateLabel("Please make sure A.V.G.M. is running in the selected game mode.", 16, 8, 315, 17)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit

    Case $modeNormal
      SetNormalMode()

    Case $modeNightmare
      SetNightmareMode()

    Case $modeHell
      SetHellMode()

    Case $modeInferno
      SetInfernoMode()

    Case $helpButton
      ; TODO: Open help form

		Case $exitButton
			Exit

    Case $startButton
      ; TODO: minimize the autoit script gui
      SetStatus($InProgressState)
      ; TODO: switch to game if inactive:
      MsgBox($MB_ICONINFORMATION, $appName, "Starting... Waiting for A.V.G.M. to become active.")
      WinWaitActive("The Basement Collection")
      start();MsgBox($MB_ICONINFORMATION, $appName, "Application Started!") ; TODO:
      SetStatus($CompletedState)

    Case $selectButton
      ; TODO: get X and Y coordinates from mouse click

	EndSwitch
WEnd

Func SetNormalMode()
  $clicks = $normalClicks
  $mode = "Normal"
  SetModeLabel()
EndFunc

Func SetNightmareMode()
  $clicks = $nightmareClicks
  $mode = "Nightmare"
  SetModeLabel()
EndFunc

Func SetHellMode()
  $clicks = $hellClicks
  $mode = "Hell"
  SetModeLabel()
EndFunc

Func SetInfernoMode()
  $clicks = $infernoClicks
  $mode = "Inferno"
  SetModeLabel()
EndFunc

Func SetModeLabel()
  $modeInfoLabel = GUICtrlCreateLabel($mode & " requires " & $clicks & " clicks to complete.", 24, 248, 214, 17)
EndFunc

Func SetStatus($status)
  $statusLabel = GUICtrlCreateLabel("Status: " & $status, 24, 272, 304, 17)
EndFunc

Func Start()
  MsgBox($MB_SYSTEMMODAL, $appName, "Click OK to start flipping the switch " & $clicks & " times for " & $mode &" mode.")

  For $clickCount = 1 To $clicks Step 1
    If _IsPressed($escapeKeyCode, $hDLL) Then
      MsgBox($MB_SYSTEMMODAL, $appName, "The Esc Key was pressed, therefore we will close the application.")
      ExitLoop
    EndIf
    MouseClick("primary" , $mouseX, $mouseY , 1 , 0)
  Next

  MsgBox($MB_ICONINFORMATION, $appName, "Done... The switch was flipped " & $clickCount & " times during " & $mode &" mode.")
EndFunc

Exit
